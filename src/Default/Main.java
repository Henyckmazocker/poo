package Default;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		
		//ArrayList contenedora de Objetos Cosos

		ArrayList<Object> o1 = new ArrayList<Object>();
		
		//Creamos Cosos

		Game g1 = new Game("PUBG", 18, "MOBA", 2);

		o1.add(g1);

		Game g2 = new Game("PUBG", 18, "MOBA", 1);

		//Creamos un Clon
		
		o1.add(g2);

		Game g3 = (Game) g1.Clone();

		o1.add(g3);

		Film f1 = new Film("PUB", 12, "fwaf", 5);

		o1.add(f1);
	
		Film f2 = new Film("BATTLE", 12, "fsa", 1);

		o1.add(f2);
		
		//Comparamos objetos Coso
		
		System.out.println(g2.compareTo(g1));
		
		System.out.println(g2.compareTo(g2));
		
		System.out.println(g2.compareTo(f1));
		
		System.out.println("\n");
		
		//Imprimimos la lista de objetos creados
		
		for (Object o : o1) {
			
			System.out.println(((Coso) o).titul + " " + ((Coso) o).empresa);
			
		}
		
		System.out.println("\n");
		
		//Iteramos la lista de Objetos y tras compararlos eliminamos los objetos que tienen nombre igual y empresa diferente

		java.util.Iterator<Object> iter = o1.iterator();

		while (iter.hasNext()) {

			Coso o = (Coso) iter.next();

			if (iter.hasNext()) {

				Coso o2 = (Coso) iter.next();

				if (o.compareTo(o2) == 1) {

					iter.remove();
					
				}
			}

		}
		
		//Imprimimos la lista tras eliminar las copias//

		for (Object o : o1) {

			System.out.println(((Coso) o).titul + " " + ((Coso) o).empresa);
		}

	}

}
