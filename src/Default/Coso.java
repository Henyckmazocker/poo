package Default;

import java.util.Comparator;


//Clase padre
public abstract class Coso implements Comparable<Coso>, Comparator<Coso>, Cloneable{

	String titul;
	int PEGI;//
	String categoria;
	int empresa;

	Coso(String titul, int PEGI, String categoria, int empresa) {

		this.titul = titul;
		this.PEGI = PEGI;
		this.categoria = categoria;
		this.empresa = empresa;

	}

	Coso() {

	}
	
	//metodos implementados de las interficies
	
	public abstract int compareTo(Coso c);
	
	public abstract int compare(Coso c1, Coso c2);
	
	public abstract Object Clone();
	
}
